<?php
//require_once('animal.php');
class Frog extends Animal
{
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function yell()
    {
        return "hop hop";
    }
}
